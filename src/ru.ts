<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <source>TR_device</source>
        <translation>Устройтсво</translation>
    </message>
    <message>
        <source>TR_bluetooth_device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <source>TR_current_rssi</source>
        <translation>Текущий RSSI (оценка расстояния)</translation>
    </message>
    <message>
        <source>TR_lock_settings</source>
        <translation>Уровни сигнала для блокировки</translation>
    </message>
    <message>
        <source>TR_unlock</source>
        <translation>Разблокировать</translation>
    </message>
    <message>
        <source>TR_lock</source>
        <translation>Блокировать</translation>
    </message>
    <message>
        <source>TR_lock_type</source>
        <translation>Способ блокировки</translation>
    </message>
    <message>
        <source>TR_manual</source>
        <translation>Ручной выбор</translation>
    </message>
    <message>
        <source>TR_lock_command</source>
        <translation>Заблокировать</translation>
    </message>
    <message>
        <source>TR_unlock_command</source>
        <translation>Раблокировать</translation>
    </message>
    <message>
        <source>TR_start</source>
        <translation>Запустить</translation>
    </message>
    <message>
        <source>TR_stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <source>TR_untrust</source>
        <translation>Не доверенный</translation>
    </message>
    <message>
        <source>TR_exit</source>
        <translation>Закрыть</translation>
    </message>
</context>
</TS>
