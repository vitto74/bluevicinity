<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <source>TR_device</source>
        <translation>Device</translation>
    </message>
    <message>
        <source>TR_bluetooth_device</source>
        <translation>Device</translation>
    </message>
    <message>
        <source>TR_current_rssi</source>
        <translation>Current RSSI (distance assessment)</translation>
    </message>
    <message>
        <source>TR_lock_settings</source>
        <translation>Signal level for lock</translation>
    </message>
    <message>
        <source>TR_unlock</source>
        <translation>Unlock</translation>
    </message>
    <message>
        <source>TR_lock</source>
        <translation>Lock</translation>
    </message>
    <message>
        <source>TR_lock_type</source>
        <translation>Locking method</translation>
    </message>
    <message>
        <source>TR_manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <source>TR_lock_command</source>
        <translation>Lock</translation>
    </message>
    <message>
        <source>TR_unlock_command</source>
        <translation>Unlock</translation>
    </message>
    <message>
        <source>TR_start</source>
        <translation>Start</translation>
    </message>
    <message>
        <source>TR_stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <source>TR_untrust</source>
        <translation>Untrusted</translation>
    </message>
    <message>
        <source>TR_exit</source>
        <translation>Close</translation>
    </message>
</context>
</TS>
