#!/usr/bin/env python3

import sys
import os
import subprocess

QT_LOAD = 0

try:
    from PyQt6 import QtCore, QtGui, QtWidgets
    from PyQt6.QtWidgets import QApplication, QMainWindow, QSystemTrayIcon, QMenu
    from PyQt6.QtCore import pyqtSignal, Qt, QObject, QTranslator, QSettings, QLocale, QFile, QDir, QSettings, QThread, QTimer, QSystemSemaphore, QSharedMemory
    from PyQt6.QtGui import QIcon, QPixmap
    from ui_main_window6 import Ui_MainWindow
    QT_LOAD = 6
except:
    pass

if QT_LOAD == 0:
    try:
        from PyQt5 import QtCore, QtGui, QtWidgets
        from PyQt5.QtWidgets import QApplication, QMainWindow, QSystemTrayIcon, QMenu, QAction
        from PyQt5.QtCore import pyqtSignal, Qt, QObject, QTranslator, QSettings, QLocale, QFile, QDir, QSettings, QThread, QTimer, QSystemSemaphore, QSharedMemory
        from PyQt5.QtGui import QIcon, QPixmap
        from ui_main_window5 import Ui_MainWindow
        QT_LOAD = 5
    except:
        pass

if QT_LOAD == 0:
    print("Can`t load PyQt6 or PyQt5 library")
    sys.exit(1)
else:
    print("Load PyQt" + str(QT_LOAD) + " library")

class MainWindow(QMainWindow, Ui_MainWindow):
    
    s_stop = pyqtSignal()
    s_start = pyqtSignal(str, int, int, str, str)
    s_mac_changed = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        ## Find icons
        self.icons = {'attention':'', 'base':'', 'nocon':'', 'pause':''}
        paths = [QDir().currentPath()+'/', '/usr/local/share/bluevicinity/', '/usr/share/bluevicinity/']
        for icon in self.icons:
            for path in paths:
                if QFile.exists(path + icon +'.svg'):
                    self.icons[icon] = QPixmap(path + icon +'.svg')
                    break

        ## Paint UI
        self.setupUi(self)
        self.tr = QtCore.QCoreApplication.translate

        ## Create tray icon
        self.tray = QtWidgets.QSystemTrayIcon(app)
        self.tray.activated.connect(self.window_show_hide)

        ## Create menu
        self.menu = QMenu(self)
        self.menu.addAction(QIcon().fromTheme('application-exit'), self.tr("MainWindow", "TR_exit"))
        self.menu.actions()[0].triggered.connect(QtWidgets.QApplication.instance().exit)
        self.tray.setContextMenu(self.menu)

        ## Set icons
        self.setIcons('pause')
        self.tray.show()
        
        ## Put device list in UI
        self.update_device_list()
        ## Put settings in UI
        self.read_settings()

        ## Create engine and move them in new thread
        self.bl = bluevicinityCore(self.combo_bluetooth_device.currentData())
        self.bl_thread = QThread()
        self.bl.s_new_rssi.connect(self.progress_current_rssi.setValue)
        self.bl.s_set_icon.connect(self.setIcons)
        self.bl.s_update_device_list.connect(self.update_device_list)
        self.bl.moveToThread(self.bl_thread)
        self.bl_thread.start()

        self.timer = QTimer()
        self.timer.timeout.connect(self.bl.main)
        self.timer.start(1000)

        self.push_start_stop.clicked.connect(self.act_start)
        self.push_ok.clicked.connect(self.act_ok)
        self.s_start.connect(self.bl.startBluevicinity)
        self.s_stop.connect(self.bl.stopBluevicinity)
        self.s_mac_changed.connect(self.bl.setMac)
        self.slider_lock.valueChanged.connect(self.act_lock_rssi_chandge)
        self.slider_unlock.valueChanged.connect(self.act_unlock_rssi_chandge)
        self.combo_lock_type.currentIndexChanged.connect(self.act_lock_type_change)
        self.combo_bluetooth_device.currentIndexChanged.connect(self.act_device_change)

        if self.autoStart:
            self.act_start()

    def setIcons(self, name):
        self.tray.setIcon(QIcon(self.icons[name]))
        self.label_status.setPixmap(QPixmap(self.icons[name]))
        self.setWindowIcon(QIcon(self.icons[name]))

    def window_show_hide(self):
        self.setVisible(not self.isVisible())

    def save_settings(self):
        self.settings.setValue('mac', self.combo_bluetooth_device.currentData())
        self.settings.setValue('lockRSSI', self.slider_lock.value())
        self.settings.setValue('unlockRSSI', self.slider_unlock.value())
        if self.combo_lock_type.currentIndex() == 0:
            self.settings.setValue('lock_type', 'logind')
        else:
            self.settings.setValue('lock_type', 'manual')
            self.settings.setValue('lock_cmd', self.line_lock_command.text())
            self.settings.setValue('unlock_cmd', self.line_unlock_command.text())

    def act_start(self):
        self.settings.setValue('autostart', str(True))

        self.group_lock_settings.setEnabled(False)
        self.combo_bluetooth_device.setEnabled(False)
        self.push_start_stop.clicked.disconnect()
        self.push_start_stop.clicked.connect(self.act_stop)
        self.push_start_stop.setText(self.tr("MainWindow", "TR_stop"))
        self.push_start_stop.setIcon(QIcon().fromTheme('media-playback-paused'))

        self.save_settings()
        self.s_start.emit(self.combo_bluetooth_device.currentData(), self.slider_lock.value(), self.slider_unlock.value(), self.line_lock_command.text(), self.line_unlock_command.text())

    def act_stop(self):
        self.settings.setValue('autostart', str(False))

        self.group_lock_settings.setEnabled(True)
        self.combo_bluetooth_device.setEnabled(True)
        self.push_start_stop.clicked.disconnect()
        self.push_start_stop.clicked.connect(self.act_start)
        self.push_start_stop.setText(self.tr("MainWindow", "TR_start"))
        self.push_start_stop.setIcon(QIcon().fromTheme('media-playback-playing'))
        self.s_stop.emit()

        self.update_device_list()

    def act_ok(self):
        self.save_settings()
        self.hide()

    def act_lock_rssi_chandge(self, value):
        if value <= self.slider_unlock.value():
            self.slider_unlock.setValue(value - 1)
            self.label_unlock_value.setText(str(value - 1))

    def act_unlock_rssi_chandge(self, value):
        if value >= self.slider_lock.value():
            self.slider_lock.setValue(value + 1)
            self.label_lock_value.setText(str(value + 1))

    def act_lock_type_change(self):
        if self.combo_lock_type.currentIndex() == 0:
            self.line_lock_command.setText('loginctl lock-session')
            self.line_lock_command.setEnabled(False)
            self.line_unlock_command.setText('loginctl unlock-session')
            self.line_unlock_command.setEnabled(False)
        else:
            self.line_lock_command.setEnabled(True)
            self.line_unlock_command.setEnabled(True)

    def act_device_change(self, index):
        self.s_mac_changed.emit(self.combo_bluetooth_device.itemData(index))

    def read_settings(self):
        self.settings = QSettings('Vitto74', 'bluevicinity')
        
        ## Autostart
        self.autoStart = self.settings.value('autostart', str(False)) == str(True)
        
        ## Device
        mac = self.settings.value('mac', '')
        item = self.combo_bluetooth_device.findData(mac)
        if item < 0:
            if self.autoStart:
                self.combo_bluetooth_device.addItem(QIcon().fromTheme('paint-unknown'), self.tr("MainWindow", "TR_untrust"), mac)
                self.combo_bluetooth_device.setCurrentIndex(self.combo_bluetooth_device.findData(mac))
        else:
            self.combo_bluetooth_device.setCurrentIndex(item)

        ## RSSI
        lockRSSI = int(self.settings.value('lockRSSI', 60))
        if lockRSSI not in range(0, 255):
            lockRSSI = 60
        unlockRSSI = int(self.settings.value('unlockRSSI', 20))
        if unlockRSSI not in range(0, 254):
            unlockRSSI = 20
        if lockRSSI <= unlockRSSI:
            lockRSSI = unlockRSSI + 1
        self.label_lock_value.setText(str(lockRSSI))
        self.slider_lock.setValue(lockRSSI)
        self.label_unlock_value.setText(str(unlockRSSI))
        self.slider_unlock.setValue(unlockRSSI)
        
        ## Lock type
        lock_type = self.settings.value('lock_type', 'logind')
        lock_cmd = ''
        unlock_cmd = ''
        if lock_type not in ['logind', 'manual']:
            lock_type = 'logind'
        if lock_type == 'manual':
            lock_cmd   = self.settings.value('lock_cmd', '')
            unlock_cmd = self.settings.value('unlock_cmd', '')
            self.group_lock_settings.setCurrentIndex(1)
        else:
            lock_cmd   = 'loginctl lock-session'
            unlock_cmd = 'loginctl unlock-session'
        if lock_cmd == '' or unlock_cmd == '':
            lock_cmd   = 'loginctl lock-session'
            unlock_cmd = 'loginctl unlock-session'
            lock_type = 'logind'
            self.group_lock_settings.setCurrentIndex(0)
        self.line_lock_command.setText(lock_cmd)
        self.line_unlock_command.setText(unlock_cmd)

    def update_device_list(self):
        ## Get truested bluetooth device
        ret_val = subprocess.run(['bluetoothctl', 'devices', 'Trusted'], stdout=subprocess.PIPE).stdout.decode('utf-8')
        count = ret_val.count('\n')
        devices = []
        devs = ret_val.split('\n')
        ## For all trusted device
        for device in devs:
            device_data = device.split(' ', 2)
            ## Save mac
            if len(device_data) == 3:
                devices.append(device_data[1])
                ## Find in combobox
                if self.combo_bluetooth_device.findData(device_data[1]) < 0:
                    ## And add if device not found
                    cmd = 'bluetoothctl info ' + device_data[1] + ' | grep -o \'Icon:.*\' | awk \'{print $2}\' | tr -d \'\n\''
                    dev_type = subprocess.run(['sh', '-c', cmd], stdout=subprocess.PIPE).stdout.decode('utf-8')
                    self.combo_bluetooth_device.addItem(QIcon().fromTheme(dev_type), device_data[2] + ' (' + device_data[1] + ')', device_data[1])
        
        if len(devices) == 0:
            self.combo_bluetooth_device.clear()
            self.combo_bluetooth_device.addItem(self.tr("MainWindow", "TR_devices_not_found"))
            self.combo_bluetooth_device.setEditable(False)

            self.combo_lock_type.setEditable(False)
            
            return
        
        ## Creating a list of unnecessary devices 
        del_dev = []
        for i in range(0, self.combo_bluetooth_device.count()):
            if self.combo_bluetooth_device.itemData(i) not in devices:
                del_dev.append(self.combo_bluetooth_device.itemData(i))
        ## And remove them
        for i in del_dev:
            self.combo_bluetooth_device.removeItem(self.combo_bluetooth_device.findData(i))

    def closeEvent(self, evnt):
        self.hide()
        self.tray.hide()
        self.settings.sync()
        self.timer.stop()
        self.bl_thread.quit()
        self.bl_thread.wait(5000)
        self.bl_thread.deleteLater()
        self.tray.deleteLater()
        super(QMainWindow, self).closeEvent(evnt)

    def changeEvent(self, event):
        if event.type() == QtCore.QEvent.Type.WindowStateChange:
            if self.windowState() == QtCore.Qt.WindowState.WindowMinimized:
                event.ignore()
                self.hide()
                return
        super(QMainWindow, self).changeEvent(event)

class bluevicinityCore(QObject):

    s_new_rssi = pyqtSignal(int)
    s_set_icon = pyqtSignal(str)
    s_update_device_list = pyqtSignal()
    
    def __init__(self, bt_mac, parent=None):
        super().__init__(parent)
        self.isWork = False
        self.isLock = False
        self.bt_mac = bt_mac
        self.lockRSSI = ''
        self.unlockRSSI = ''
        self.lock_cmd = ''
        self.unlock_cmd = ''

        self.devices = ''
        self.rssi_event_count = 0

        self.devListRaw = ret_val = subprocess.run(['bluetoothctl', 'devices', 'Trusted'], stdout=subprocess.PIPE).stdout.decode('utf-8')

    def setMac(self, bt_mac):
        self.bt_mac = bt_mac

    def startBluevicinity(self, bt_mac, lock_rssi, unlock_rssi, lock_cmd, unlock_cmd):
        self.bt_mac = bt_mac
        self.lockRSSI = lock_rssi
        self.unlockRSSI = unlock_rssi
        self.lock_cmd = lock_cmd
        self.unlock_cmd = unlock_cmd
        self.isWork = True
        self.isLock = False
        self.s_set_icon.emit('base')

    def stopBluevicinity(self):
        self.isWork = False
        self.s_set_icon.emit('pause')

    def main(self):
        ret = subprocess.run(['sh', '-c', 'hcitool rssi ' + self.bt_mac + ' 2> /dev/null'], stdout=subprocess.PIPE).stdout.decode('utf-8')
        strs = ret.split(':')
        ret = -255
        if len(strs) > 1:
            ret = strs[1].strip(' ')
        rssi = int(ret) * -1
        self.s_new_rssi.emit(rssi)

        if self.isWork:
            subprocess.run(['bluetoothctl', '--timeout', '1', 'connect', self.bt_mac], stdout=subprocess.PIPE)
            if self.isLock:
                if rssi <= self.unlockRSSI:
                    self.rssi_event_count = self.rssi_event_count + 1
                else:
                    self.rssi_event_count = 0
                if self.rssi_event_count > 2:
                    self.unlock()
                    self.s_set_icon.emit('base')
                    self.rssi_event_count = 0
            else:
                if rssi >= self.lockRSSI:
                    self.rssi_event_count = self.rssi_event_count + 1
                    self.s_set_icon.emit('attention')
                else:
                    self.rssi_event_count = 0
                    self.s_set_icon.emit('base')
                if self.rssi_event_count > 2:
                    self.lock()
                    self.s_set_icon.emit('nocon')
                    self.rssi_event_count = 0
        else:
            newDevListRaw = ret_val = subprocess.run(['bluetoothctl', 'devices', 'Trusted'], stdout=subprocess.PIPE).stdout.decode('utf-8')
            if self.devListRaw != newDevListRaw:
                self.devListRaw = newDevListRaw
                self.s_update_device_list.emit()

    def lock(self):
        self.isLock = True
        subprocess.run(['sh', '-c', self.lock_cmd], stdout=subprocess.PIPE)

    def unlock(self):
        self.isLock = False
        subprocess.run(['sh', '-c', self.unlock_cmd], stdout=subprocess.PIPE)

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    window_id = 'Bluevicinity_wid'
    shared_mem_id = 'Bluevicinity_mem'
    semaphore = QSystemSemaphore(window_id, 1)
    semaphore.acquire()
    nix_fix_shared_mem = QSharedMemory(shared_mem_id)
    if nix_fix_shared_mem.attach():
        nix_fix_shared_mem.detach()
    shared_memory = QSharedMemory(shared_mem_id)
    if shared_memory.attach():
        is_running = True
    else:
        shared_memory.create(1)
        is_running = False
    semaphore.release()

    if is_running:
        sys.exit(0)
    else:
        ## Load translate
        lang = QLocale().system().name()[:QLocale().system().name().find('_')]
        trans = QtCore.QTranslator(app)
        if not trans.load(lang):
            trans.load('en')
        QtWidgets.QApplication.instance().installTranslator(trans)

        win = MainWindow()
        if not win.autoStart:
            win.show()
        sys.exit(app.exec())
