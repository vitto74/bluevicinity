# BlueVicinity

BlueVicinity helps you adding a little more security to your desktop. It does so by detecting one of your bluetooth devices, most likely your mobile phone, and keeping track of its distance. If you move away from your computer and the distance is above a certain level (no measurement in meters is possible) for a given time, it automatically locks your desktop (or starts any other shell command you want).

Once away, if you return nearer than a given level for a set time your computer unlocks magically without any interaction (or starts any other shell command you want).

BlueVicinity is inspired by the [BlueProximity](https://sourceforge.net/projects/blueproximity/) program, abandoned in 2013 and borrows an idea, license and icons from it.

# Use

Launch and select a bluetooth device from the trusted list. Establish a connection between the PC and the device. When the connection is established, the distance to the device in a conditional value (RSSI) will be displayed. Select the RSSI value at which to lock the screen and at which to unlock. Click OK. The application will collapse into the tray. The next time you start it, it will work immediately and lock the screen if the selected device is far away or unavailable.

By default, the application uses logind to lock/unlock the screen, but you can specify any shell command. To do this, select "Manual" in the locking method field and set the desired commands.

# Build

## Build requirements ##
 - pyuic5 from pyqt5-dev-tools or pyuic6 from pyqt6-dev-tools
 - lrelease from qtchooser
 - build-essential for building a deb package

## Start requirements ##
 - Python3
 - PyQt5 from python3-pyqt5 or PyQt6 from python3-pyqt6

The application works equally with Qt5 and Qt6, so it is advisable to build both versions of the interface. You can use the make command to build and install, but it is not recommended to install using make.

To build a deb package, use the script create_deb.sh

There is no script for building an rpm package yet. Perhaps it will appear in the future.