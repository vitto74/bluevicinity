PREFIX=/usr
DATA_PATH=$(PREFIX)/share/bluevicinity
VERSION=0.9

all: build

build: ui5 ui6 qm run_script desktop_file

ui5:
	pyuic5 src/main_window.ui -o src/ui_main_window5.py

ui6:
	pyuic6 src/main_window.ui -o src/ui_main_window6.py

qm:
	lrelease src/*.ts

run_script:
	echo '#!/bin/sh' > src/bluevicinity
	echo "cd $(DATA_PATH)" >> src/bluevicinity
	echo "python3 $(DATA_PATH)/bluevicinity.py" >> src/bluevicinity

desktop_file:
	cat src/bluevicinity_dev.desktop > src/bluevicinity.desktop
	echo "Exec=$(PREFIX)/bin/bluevicinity" >> src/bluevicinity.desktop
	echo "Icon=$(PREFIX)/share/pixmaps/bluevicinity.svg" >> src/bluevicinity.desktop

clean:
	rm -f src/*.qm
	rm -f src/ui_main_window5.py
	rm -f src/ui_main_window6.py
	rm -f src/bluevicinity
	rm -f src/bluevicinity.desktop

install: all
	-mkdir -p $(DESTDIR)$(DATA_PATH)/
	-install -d $(DESTDIR)$(PREFIX)/bin
	install -m 755 src/bluevicinity $(DESTDIR)$(PREFIX)/bin/
	install -m 755 src/*.py $(DESTDIR)$(DATA_PATH)/
	install -m 644 src/*.qm $(DESTDIR)$(DATA_PATH)/
	install -m 644 src/*.svg $(DESTDIR)$(DATA_PATH)/
	-install -d $(DESTDIR)$(PREFIX)/share/pixmaps
	install -m 644 src/base.svg $(DESTDIR)$(PREFIX)/share/pixmaps/bluevicinity.svg
	-install -d $(DESTDIR)$(PREFIX)/share/applications
	install -m 755 src/bluevicinity.desktop $(DESTDIR)$(PREFIX)/share/applications
	-install -d $(DESTDIR)/etc/xdg/autostart
	install -m 755 src/bluevicinity.desktop $(DESTDIR)/etc/xdg/autostart

uninstall:
	-rm -f $(PREFIX)/bin/bluevicinity
	-rm -f $(DATA_PATH)/*.py
	-rm -f $(DATA_PATH)/*.qm
	-rm -f $(DATA_PATH)/*.svg
	-rmdir $(DATA_PATH)/
	-rm -f $(PREFIX)/share/pixmaps/bluevicinity.svg
	-rm -f $(PREFIX)/share/applications/bluevicinity.desktop
	-rm -f /etc/xdg/autostart/bluevicinity.desktop
	